#editors
*.swp
.buildpath
.project
.settings
nbproject
.idea
*.sublime-project
*sublime-workspace

#general
.gitignore
.htaccess
*.log

#personal
app_dev.php
index-test.php

#folders
assets/*
protected/runtime/*
#protected/data/*.db
protected/tests/report/*


---------------

{
	"binary_file_patterns":
	[
		"*.jpg",
		"*.jpeg",
		"*.png",
		"*.gif",
		"*.ttf",
		"*.tga",
		"*.dds",
		"*.ico",
		"*.eot",
		"*.pdf",
		"*.swf",
		"*.jar",
		"*.zip",
		"*.swp"
	],
	"color_scheme": "Packages/Color Scheme - Default/Pastels on Dark.tmTheme",
	"default_line_ending": "unix",
	"file_exclude_patterns":
	[
		"*.pyc",
		"*.pyo",
		"*.exe",
		"*.dll",
		"*.obj",
		"*.o",
		"*.a",
		"*.lib",
		"*.so",
		"*.dylib",
		"*.ncb",
		"*.sdf",
		"*.suo",
		"*.pdb",
		"*.idb",
		".DS_Store",
		"*.class",
		"*.psd",
		"*.db",
		".*",
		"*.tar",
		"*.gz",
		"*.zip",
		"*.sublime-project",
		"*.sublime-workspace"
	],
	"folder_exclude_patterns":
	[
		".svn",
		".idea",
		".git",
		".hg",
		"CVS",
		"assets",
		"images",
		"nbproject",
		".settings"
	],
	"font_face": "Consolas",
	"font_size": 12.0,
	"highlight_line": true,
	"ignored_packages":
	[
		"Vintage"
	],
	"line_padding_bottom": 2,
	"line_padding_top": 1,
	"phoenix_highlight_current_tab": true,
	"save_on_focus_lost": true,
	"scroll_speed": 0,
	"show_tab_close_buttons": false,
	"soda_classic_tabs": false,
	"tab_size": 4,
	"theme": "Soda Light.sublime-theme",
	"translate_tabs_to_spaces": true,
	"tree_animation_enabled": false,
	"trim_trailing_white_space_on_save": true,
	"word_wrap": true
}
