<?php
$this->breadcrumbs=array(
	'Libros'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Libro', 'url'=>array('index')),
	array('label'=>'Create Libro', 'url'=>array('create')),
	array('label'=>'View Libro', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Libro', 'url'=>array('admin')),
);
?>

<h1>Update Libro <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>