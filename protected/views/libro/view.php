<?php
$this->breadcrumbs=array(
	'Libros'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Libro', 'url'=>array('index')),
	array('label'=>'Create Libro', 'url'=>array('create')),
	array('label'=>'Update Libro', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Libro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Libro', 'url'=>array('admin')),
);
?>

<h1>View Libro #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'titulo',
		'user_id',
	),
)); ?>
