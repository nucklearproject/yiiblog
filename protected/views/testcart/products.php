<pre>
CODE:	var_dump(Yii::app()->cart->contents());
</pre>
<?php var_dump(Yii::app()->cart->contents()); ?>
<hr />
<?php echo CHtml::form(); ?>


<table cellpadding="6" cellspacing="1" style="width:100%" border="0">

<tr  style="background-color: #ccc;">
  <th>QTY</th>
  <th>Item Description</th>
  <th style="text-align:right">Item Price</th>
  <th style="text-align:right">Sub-Total</th>
</tr>

<?php $i = 1; ?>

<?php foreach (Yii::app()->cart->contents() as $items): ?>

	<?php // echo form_hidden($i.'[rowid]', $items['rowid']); ?>

	<tr>
	<td><?php echo CHtml::textField($i.'[qty]', $items['qty'], array('maxlength' => '3', 'size' => '5')); ?></td>
	 </td>
	  <td>
		<?php echo $items['name']; ?>

			<?php if (Yii::app()->cart->has_options($items['rowid']) == TRUE): ?>

				<p>
					<?php foreach (Yii::app()->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

						<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

					<?php endforeach; ?>
				</p>

			<?php endif; ?>

	  </td>
	  <td style="text-align:right"><?php echo Yii::app()->cart->format_number($items['price']); ?></td>
	  <td style="text-align:right">$<?php echo Yii::app()->cart->format_number($items['subtotal']); ?></td>
	</tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr  style="background-color: #ccc;">
  <td colspan="2"> </td>
  <td style="text-align:right"><strong>Total</strong></td>
  <td style="text-align:right"> $<?php echo Yii::app()->cart->format_number(Yii::app()->cart->total()); ?></td>
</tr>

</table>

<div class="row buttons">
		<?php echo CHtml::submitButton('Update Cart'); ?>
</div>

<?php CHtml::endForm(); ?>

