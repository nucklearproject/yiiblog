<?php

class TestcartController extends Controller
{

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    public function actionIndex()
    {

        $data = array('id'      => 'sku_123ABC',
                      'qty'     => 2,
                      'price'   => 39.00,
                      'name'    => 'T-Shirt',
                      'options' => array('Size'  => 'L',
                                         'Color' => 'Red'));

        $data2 = array('id'      => 'sku_123ABCJJJ',
                       'qty'     => 1,
                       'price'   => 39.00,
                       'name'    => 'T-Shirt',
                       'options' => array('Size'  => 'M',
                                          'Color' => 'Red'));

        $data3 = array('id' => 'sku_123ABCJJJDDD', 'qty' => 1, 'price' => 39.00, 'name' => 'T-Shirt', 'options' => array('Size' => 'M', 'Color' => 'Red'));

        $update = array('rowid' => '0256a32c98ce49afbe2a4eb8c96c5884', 'qty' => 5);
        //$cart = new SimpleCart();
        Yii::app()->cart->insert($data);
        Yii::app()->cart->insert($data2);
        Yii::app()->cart->insert($data3);


        //Yii::app()->cart->destroy();
        //Yii::app()->session['uno'] = $data;
        //var_dump(Yii::app()->session['uno']);
        //var_dump(Yii::app()->cart->contents());

        //echo "<hr>";
        //Yii::app()->cart->update($update);
        //var_dump(Yii::app()->session['cart_contents']);

        //echo "<hr>";
        //echo Yii::app()->cart->format_number(Yii::app()->cart->total());

    }

    public function actionList()
    {

        $this->render('products');

    }

    public function update()
    {

    }

}
